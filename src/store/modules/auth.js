import { AUTH_LOGIN, AUTH_LOGOUT } from '../mutation-types'
import router from '../../router'
// import api from '../../service/api'
import { login } from '../../service/auth'
export default {
  namespaced: true,
  state: () => ({
    user: JSON.parse(localStorage.getItem('user'))

  }),
  mutations: {
    [AUTH_LOGIN] (state, payload) {
      state.user = payload
    },
    [AUTH_LOGOUT] (state) {
      state.user = null
    }
  },
  actions: {
    async login ({ commit }, payload) {
      console.log(payload)
      const res = await login(payload.email, payload.password)
      const user = res.data.user
      const token = res.data.token
      localStorage.setItem('token', token)
      localStorage.setItem('user', JSON.stringify(user))
      console.log(res)

      router.push('/')
      commit(AUTH_LOGIN, user)
    },
    logout ({ commit }) {
      localStorage.removeItem('token')
      localStorage.removeItem('user')
      commit(AUTH_LOGOUT)
    }
  },
  getters: {
    isLogin (state, getter) {
      return state.user != null
    }
  }
}
